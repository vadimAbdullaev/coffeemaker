using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.EventHubs;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RuleEngine.ActionExecutor;
using RuleEngine.ConditionEvaluator.Contracts;
using RuleEngine.Manager.Models;
using RuleEngine.Manager.Repository;

namespace RuleEngine.Manager
{
    public class RuleEnginManager
    {
        private readonly IConditionEngine _conditionEngine;
        private readonly IActionEngine _actionEngine;
        private readonly IRuleRepository _ruleRepository;

        public RuleEnginManager(IConditionEngine conditionEngine, IActionEngine actionEngine, IRuleRepository ruleRepository)
        {
            _conditionEngine = conditionEngine;
            _actionEngine = actionEngine;
            _ruleRepository = ruleRepository;
        }

        [FunctionName(nameof(RuleEnginManager))]
        public async Task Run([EventHubTrigger("CoffeMaker", Connection = "EventHubConnectionString")] EventData[] events, ILogger log)
        {
            var exceptions = new List<Exception>();

            foreach (EventData eventData in events)
            {
                try
                {
                    string messageBody = Encoding.UTF8.GetString(eventData.Body.Array, eventData.Body.Offset, eventData.Body.Count);
                    var message = JsonConvert.DeserializeObject<EventMessage>(messageBody);

                    foreach (var rule in _ruleRepository.Rules)
                        if (_conditionEngine.Evaluate(rule.Condition, message))
                        {
                            var actionExecutor = _actionEngine.Execute(rule.Action);
                            actionExecutor.Execute();
                        }

                    // Replace these two lines with your processing logic.
                    log.LogInformation($"C# Event Hub trigger function processed a message: {messageBody}");
                    await Task.Yield();
                }
                catch (Exception e)
                {
                    // We need to keep processing the rest of the batch - capture this exception and continue.
                    // Also, consider capturing details of the message that failed processing so it can be processed again later.
                    exceptions.Add(e);
                }
            }

            // Once processing of the batch is complete, if any messages in the batch failed processing throw an exception so that there is a record of the failure.

            if (exceptions.Count > 1)
                throw new AggregateException(exceptions);

            if (exceptions.Count == 1)
                throw exceptions.Single();
        }
    }
}
