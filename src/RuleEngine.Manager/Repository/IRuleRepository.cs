﻿using RuleEngine.Manager.Models;
using System.Collections.Generic;

namespace RuleEngine.Manager.Repository
{
    public interface IRuleRepository
    {
         IList<Rule> Rules { get; set; }
    }
}