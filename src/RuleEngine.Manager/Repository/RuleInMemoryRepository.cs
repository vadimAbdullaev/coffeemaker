﻿using RuleEngine.ActionExecutor.Actions;
using RuleEngine.Conditions;
using RuleEngine.Manager.Models;
using System.Collections.Generic;

namespace RuleEngine.Manager.Repository
{
    internal class RuleInMemoryRepository : IRuleRepository
    {
        public IList<Rule> Rules { get; set; }

        public RuleInMemoryRepository()
        {
            Rules = new List<Rule>
            {
                new Rule
                {
                    Condition = new WasteContainerNeedsToBeEmptiedCondition(70),
                    Action = new SendSmsAction()
                }
            };
        }
    }
}
