﻿using RuleEngine.ActionExecutor.Contracts;
using RuleEngine.ConditionEvaluator.Contracts;

namespace RuleEngine.Manager.Models
{
    public class Rule : IRule
    {
        public ICondition Condition { get; set; }
        public IAction Action { get; set; }
    }
}
