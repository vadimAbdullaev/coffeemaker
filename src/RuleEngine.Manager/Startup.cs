﻿using Microsoft.Azure.Functions.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using RuleEngine.ConditionEvaluator;
using RuleEngine.ConditionEvaluator.Contracts;
using RuleEngine.Manager.Repository;

[assembly: FunctionsStartup(typeof(RuleEngine.Manager.Startup))]
namespace RuleEngine.Manager
{
    public class Startup : FunctionsStartup
    {
        public override void Configure(IFunctionsHostBuilder builder)
        {
            builder.Services.AddScoped<IConditionEngine, ConditionEngine>();
            builder.Services.AddScoped<IRuleRepository, RuleInMemoryRepository>();
        }
    }
}
