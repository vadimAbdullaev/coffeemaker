﻿using RuleEngine.ConditionEvaluator.Contracts;
using System;

namespace RuleEngine.Manager.Models
{
    public class EventMessage : IEventMessage
    {
        public int WasteContainerFullness { get; set; }
        public DateTimeOffset LastMaintenanceDate { get; set; }
    }
}
