﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CoffeeMaker.Infrastructure.Data.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CoffeeMachines",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    SerialNumber = table.Column<string>(nullable: false),
                    MachineType = table.Column<string>(nullable: true),
                    MaintenanceNumber = table.Column<int>(nullable: false),
                    LastMaintenanceDate = table.Column<DateTimeOffset>(nullable: true),
                    MaintenanceEmployee = table.Column<Guid>(nullable: false),
                    WasteContainerFullness = table.Column<short>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    NotInErrorStatus = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CoffeeMachines", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CoffeeMachines");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
