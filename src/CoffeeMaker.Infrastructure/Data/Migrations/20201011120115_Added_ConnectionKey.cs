﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CoffeeMaker.Infrastructure.Data.Migrations
{
    public partial class Added_ConnectionKey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ConnectionKey",
                table: "CoffeeMachines",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ConnectionKey",
                table: "CoffeeMachines");
        }
    }
}
