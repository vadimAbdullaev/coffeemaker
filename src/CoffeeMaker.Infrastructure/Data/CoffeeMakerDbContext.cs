﻿using CoffeeMaker.Core.Contracts;
using CoffeeMaker.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace CoffeeMaker.Infrastructure.Data
{
    public class CoffeeMakerDbContext : DbContext, IUnitOfWork
    {
        public CoffeeMakerDbContext(DbContextOptions<CoffeeMakerDbContext> options) : base(options) { }

        public DbSet<User> Users { get; set; }
        public DbSet<CoffeeMachine> CoffeeMachines { get; set; }

        public IDbContextTransaction BeginTransaction()
        {
            return Database.BeginTransaction();
        }
    }
}
