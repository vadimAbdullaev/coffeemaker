﻿using CoffeeMaker.Core.Commands.CoffeeMachines;
using CoffeeMaker.Core.Queries.CoffeeMachines;
using CoffeeMaker.Dto;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CoffeeMaker.Api.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class CoffeeMachineController : ControllerBase
    {
        private readonly IMediator _mediator;

        public CoffeeMachineController(IMediator mediator)
        {
            _mediator = mediator;
        }

        // GET: api/<CoffeeMachineController>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var coffeeMachines = await _mediator.Send(new GetAllCoffeeMachinesQuery());
            return Ok(coffeeMachines);
        }

        // GET api/<CoffeeMachineController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var coffeMachine = await _mediator.Send(new GetCoffeeMachineQuery { CoffeeMachineId = id });
            return Ok(coffeMachine);
        }

        // POST api/<CoffeeMachineController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CoffeeMachineDto coffeeMakerDto)
        {
            await _mediator.Send(new CreateCoffeeMachineCommand
            {
                CoffeeMachineDto = coffeeMakerDto
            });

            return Ok();
        }

        // PUT api/<CoffeeMachineController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, [FromBody] CoffeeMachineDto coffeMachineDto)
        {
            await _mediator.Send(new UpdateCoffeeMachineCommand
            {
                CoffeeMachineDto = coffeMachineDto
            });

            return Ok();
        }

        // DELETE api/<CoffeeMachineController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            await _mediator.Send(new DeleteCoffeeMachineCommand { CoffeeMachineId = id });
            return Ok();
        }
    }
}
