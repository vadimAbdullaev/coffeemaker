using CoffeeMaker.Infrastructure.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using System.Linq;

namespace CoffeMaker.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();
            RunMigrationIfNeeded(args, host);

            host.Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });

        private static void RunMigrationIfNeeded(string[] args, IHost host)
        {
            if (args.Any(x => x == "/migrate"))
            {
                using var scope = host.Services.CreateScope();
                var context = scope.ServiceProvider.GetService<CoffeeMakerDbContext>();
                context.Database.Migrate();
            }
        }
    }
}
