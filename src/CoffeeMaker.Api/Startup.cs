using AutoMapper;
using CoffeeMaker.Api.Options;
using CoffeeMaker.Core;
using CoffeeMaker.Core.Contracts;
using CoffeeMaker.Core.Models;
using CoffeeMaker.Core.Services;
using CoffeeMaker.Infrastructure.Data;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;

namespace CoffeMaker.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMediatR(typeof(AssemblyMarker));
            services.AddAutoMapper(typeof(Startup), typeof(AssemblyMarker));

            services.AddScoped<IUnitOfWork, CoffeeMakerDbContext>();

            services.Configure<AzureIotHubOptions>(Configuration.GetSection("AzureIotHubOptions"));
            services.AddScoped<IAzureDeviceService, AzureDeviceService>();

            services.AddDbContext<CoffeeMakerDbContext>(options =>
                options.UseSqlite(Configuration.GetConnectionString("DefaultConnection")));

            //services.AddAuthentication(OpenIdConnectDefaults.AuthenticationScheme)
            //    .AddMicrosoftIdentityWebApp(Configuration.GetSection("AzureAd"));

            var adOptions = Configuration.GetSection("AzureAd").Get<AzureAd>();
            services.AddAuthentication(sharedOptions =>
            {
                sharedOptions.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options =>
            {
                options.RequireHttpsMetadata = true;
                options.Audience = adOptions.ClientId;
                options.Authority = $"{adOptions.Instance}/{adOptions.TenantId}/v2.0";
            });

            services.AddCors(options =>
                {
                    var allowedHostsSection = Configuration.GetSection("AllowedHosts");
                    var allowedHosts = allowedHostsSection.Get<string[]>();

                    options.AddPolicy("default", policy =>
                        {
                            policy.WithOrigins(allowedHosts)
                                .AllowAnyHeader()
                                .AllowAnyMethod()
                                .AllowCredentials();
                        });
                });

            services.AddSwaggerGen(setup =>
            {
                setup.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
                {
                    Type = SecuritySchemeType.OAuth2,
                    Description = "Api Authentication",

                    Flows = new OpenApiOAuthFlows
                    {
                        Implicit = new OpenApiOAuthFlow
                        {
                            AuthorizationUrl = new Uri($"{adOptions.Instance}/{adOptions.TenantId}/oauth2/v2.0/authorize"),
                            TokenUrl = new Uri($"{adOptions.Instance}/{adOptions.TenantId}/oauth2/v2.0/token"),
                            
                            Scopes = new Dictionary<string, string>
                            {
                               { "openid", "Sign In Permissions" },
                               { "profile", "User Profile Permissions" },
                               { $"api://{adOptions.ClientId}/swag", "swag" }
                            }
                        }
                    }
                });
            });

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "CoffeeMaker V1");
            });
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors("default");
            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
