﻿using CoffeeMaker.Core.Commands.CoffeeMachines;
using CoffeeMaker.Core.Contracts;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace CoffeeMaker.Core.CommandHandlers.CoffeeMachines
{
    public class DeleteCoffeeMachineCommandHandler : IRequestHandler<DeleteCoffeeMachineCommand, bool>
    {
        private readonly IUnitOfWork _unitOfWork;

        public DeleteCoffeeMachineCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<bool> Handle(DeleteCoffeeMachineCommand request, CancellationToken cancellationToken)
        {
            var coffeeMachineRepo = _unitOfWork.CoffeeMachines;
            var coffeeMachine = await coffeeMachineRepo.FirstOrDefaultAsync(x => x.Id == request.CoffeeMachineId);

            if (coffeeMachine == null)
                return false;

            coffeeMachineRepo.Remove(coffeeMachine);

            await _unitOfWork.SaveChangesAsync();
            
            return true;
        }
    }
}
