﻿using CoffeeMaker.Core.Commands.CoffeeMachines;
using CoffeeMaker.Core.Contracts;
using CoffeeMaker.Domain;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace CoffeeMaker.Core.CommandHandlers.CoffeMachines
{
    public class CreateCoffeeMachineCommandHandler : IRequestHandler<CreateCoffeeMachineCommand, Guid?>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IAzureDeviceService _azureDeviceService;

        public CreateCoffeeMachineCommandHandler(IUnitOfWork unitOfWork, IAzureDeviceService azureDeviceService)
        {
            _unitOfWork = unitOfWork;
            _azureDeviceService = azureDeviceService;
        }

        public async Task<Guid?> Handle(CreateCoffeeMachineCommand request, CancellationToken cancellationToken)
        {
            var dto = request.CoffeeMachineDto;

            var newMachine = new Domain.CoffeeMachine(dto.SerialNumber, dto.MachineType, dto.MaintenanceEmployee);

            await _unitOfWork.CoffeeMachines.AddAsync(newMachine);
            await _unitOfWork.SaveChangesAsync();

            var device = await _azureDeviceService.Register(newMachine.SerialNumber);

            var key = device.Authentication.SymmetricKey.SecondaryKey;
            await UpdateConnectionKey(newMachine, key);
            return newMachine.Id;
        }

        private async Task UpdateConnectionKey(CoffeeMachine newMachine, string key)
        {
            _unitOfWork.CoffeeMachines.Attach(newMachine);

            newMachine.ConnectionKey = key;
            _unitOfWork.CoffeeMachines.Update(newMachine);

            await _unitOfWork.SaveChangesAsync();
        }
    }
}
