﻿using CoffeeMaker.Core.Commands.CoffeeMachines;
using CoffeeMaker.Core.Contracts;
using CoffeeMaker.Core.Exceptions;
using CoffeeMaker.Domain;
using CoffeeMaker.Dto;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace CoffeeMaker.Core.CommandHandlers.CoffeeMachines
{
    public class UpdateCoffeeMachineCommandHandler : IRequestHandler<UpdateCoffeeMachineCommand>
    {
        private readonly IUnitOfWork _unitOfWork;

        public UpdateCoffeeMachineCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Unit> Handle(UpdateCoffeeMachineCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var repo = _unitOfWork.CoffeeMachines;
                var coffeeMachine = await repo.FirstAsync(x => x.Id == request.CoffeeMachineDto.Id);
                UpdateModel(coffeeMachine, request.CoffeeMachineDto);

                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new BusinessException($"Update failled", ex);
            }

            return Unit.Value;
        }

        private void UpdateModel(CoffeeMachine coffeeMachine, CoffeeMachineDto coffeeMachineDto)
        {
            coffeeMachine.ChangeMaintenanceEmployee(coffeeMachineDto.MaintenanceEmployee);
            coffeeMachine.ChangeStatus(coffeeMachineDto.Status);
            coffeeMachine.LastMaintenanceDate = coffeeMachineDto.LastMaintenanceDate;
            coffeeMachine.MachineType = coffeeMachineDto.MachineType;
            coffeeMachine.MaintenanceNumber = coffeeMachineDto.MaintenanceNumber;
            coffeeMachine.NotInErrorStatus = coffeeMachineDto.NotInErrorStatus;
            coffeeMachine.WasteContainerFullness = coffeeMachineDto.WasteContainerFullness;
        }
    }
}
