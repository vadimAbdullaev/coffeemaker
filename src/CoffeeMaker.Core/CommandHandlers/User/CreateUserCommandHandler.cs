﻿using CoffeeMaker.Core.Commands.Users;
using CoffeeMaker.Core.Contracts;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace CoffeeMaker.Core.CommandHandlers.User
{
    public class CreateUserCommandHandler : IRequestHandler<CreateUserCommand>
    {
        private readonly IUnitOfWork _unitOfWork;

        public CreateUserCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Unit> Handle(CreateUserCommand request, CancellationToken cancellationToken)
        {
            var userDto = request.UserDto;
            var user = new Domain.User(userDto.FirstName, userDto.LastName, userDto.Email);

            await _unitOfWork.Users.AddAsync(user);
            await _unitOfWork.SaveChangesAsync();
            return Unit.Value;
        }
    }
}
