﻿using AutoMapper;
using CoffeeMaker.Core.Models.Dto;
using CoffeeMaker.Domain;
using CoffeeMaker.Dto;

namespace CoffeeMaker.Core.AutoMapperProfiles
{
    public class Profiles : Profile
    {
        public Profiles()
        {
            CreateMap<User, UserDto>();
            CreateMap<CoffeeMachine, CoffeeMachineDto>();
        }
    }
}
