﻿using Microsoft.Azure.Devices;
using System.Threading.Tasks;

namespace CoffeeMaker.Core.Contracts
{
    public interface IAzureDeviceService
    {
        Task<Device> Register(string deviceId);
        Task<Device> GetDevice(string deviceId);
        Task DeleteDevice(string deviceId);
    }
}
