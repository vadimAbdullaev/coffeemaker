﻿using CoffeeMaker.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System.Threading;
using System.Threading.Tasks;

namespace CoffeeMaker.Core.Contracts
{
    public interface IUnitOfWork
    {
        public DbSet<User> Users { get; set; }
        public DbSet<CoffeeMachine> CoffeeMachines { get; set; }

        int SaveChanges();

        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);

        IDbContextTransaction BeginTransaction();
    } 
}
