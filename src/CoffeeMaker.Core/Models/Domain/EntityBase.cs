﻿using System;

namespace CoffeeMaker.Domain
{
    public class EntityBase
    {
        public Guid Id { get; set; }
    }
}