﻿using CoffeeMaker.Models;
using System;

namespace CoffeeMaker.Domain
{
    public class CoffeeMachine : EntityBase
    {
        private short _wasteContainerFullness;

        public string SerialNumber { get; private set; }
        public string MachineType { get; set; }
        public int MaintenanceNumber { get; set; }
        public DateTimeOffset LastMaintenanceDate { get; set; }
        public Guid MaintenanceEmployee { get; private set; }
        public short WasteContainerFullness
        {
            get => _wasteContainerFullness;
            set
            {
                if (0 <= value && value <= 100)
                    _wasteContainerFullness = value;
                else
                    throw new ArgumentOutOfRangeException(nameof(WasteContainerFullness));
            }
        }
        public CoffeeMachineStatus Status { get; private set; }
        public bool NotInErrorStatus { get; set; }
        public string ConnectionKey { get; internal set; }

        protected CoffeeMachine() { }

        public CoffeeMachine(string serialNumber, string machineType, Guid maintenanceEmployee)
        {
            if (string.IsNullOrEmpty(serialNumber))
                throw new ArgumentException($"'{nameof(serialNumber)}' cannot be null or empty", nameof(serialNumber));
            if (Guid.Empty == maintenanceEmployee)
                throw new ArgumentException($"'{nameof(maintenanceEmployee)}' cannot be null or empty", nameof(maintenanceEmployee));
            if (string.IsNullOrEmpty(machineType))
                throw new ArgumentException($"'{nameof(machineType)}' cannot be null or empty", nameof(machineType));

            ConnectionKey = "_INVALID_KEY";
            Id = Guid.NewGuid();
            LastMaintenanceDate = DateTimeOffset.UtcNow;
            MachineType = machineType;
            MaintenanceEmployee = maintenanceEmployee;
            MaintenanceNumber = 0;
            NotInErrorStatus = true;
            SerialNumber = serialNumber;
            Status = CoffeeMachineStatus.Ready;
            WasteContainerFullness = 0;
        }

        public void ChangeStatus(CoffeeMachineStatus status)
        {
            Status = status;
        }

        public void ChangeMaintenanceEmployee(Guid maintenanceEmployeeId)
        {
            MaintenanceEmployee = maintenanceEmployeeId;
        }
    }
}
