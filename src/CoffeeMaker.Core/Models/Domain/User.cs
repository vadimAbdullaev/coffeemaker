﻿using System;

namespace CoffeeMaker.Domain
{
    public class User : EntityBase
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }

        protected User() { }

        public User(string firstName, string lastName, string email)
        {
            if (string.IsNullOrEmpty(firstName))
                throw new System.ArgumentException($"'{nameof(firstName)}' cannot be null or empty", nameof(firstName));

            if (string.IsNullOrEmpty(lastName))
                throw new System.ArgumentException($"'{nameof(lastName)}' cannot be null or empty", nameof(lastName));

            if (string.IsNullOrEmpty(email))
                throw new System.ArgumentException($"'{nameof(email)}' cannot be null or empty", nameof(email));

            Id = Guid.NewGuid();
            FirstName = firstName;
            LastName = lastName;
            Email = email;
        }
    }
}
