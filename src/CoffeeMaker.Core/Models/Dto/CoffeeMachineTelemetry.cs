﻿using CoffeeMaker.Models;
using System;

namespace CoffeeMaker.Dto
{
    public class CoffeeMachineTelemetry
    {
        public string SerialNumber { get; set; }
        public string MachineType { get; set; }
        public int MaintenanceNumber { get; set; }
        public DateTimeOffset LastMaintenanceDate { get; set; }
        public string MaintenanceEmployee { get; set; }
        public short WasteContainerFullness { get; set; }
        public CoffeeMachineStatus Status { get; set; }
        public bool NotInErrorStatus { get; set; }
    }
}
