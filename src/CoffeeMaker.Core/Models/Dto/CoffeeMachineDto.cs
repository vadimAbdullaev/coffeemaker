﻿using CoffeeMaker.Models;
using System;

namespace CoffeeMaker.Dto
{
    public class CoffeeMachineDto
    {
        public Guid Id { get; set; }
        public string SerialNumber { get; set; }
        public string MachineType { get; set; }
        public Guid MaintenanceEmployee { get; set; }
        public DateTimeOffset LastMaintenanceDate { get; set; }
        public int MaintenanceNumber { get; set; }
        public bool NotInErrorStatus { get; set; }
        public CoffeeMachineStatus Status { get; set; }
        public short WasteContainerFullness { get; set; }
        public string ConnectionKey { get; set; }
    }
}
