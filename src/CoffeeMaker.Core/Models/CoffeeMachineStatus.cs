﻿namespace CoffeeMaker.Models
{
    public enum CoffeeMachineStatus
    {
        Initializing = 1,
        ServiceMode,
        HeatingUp,
        Ready,
        Preparing,
        Serving,
        Done
    }
}
