﻿namespace CoffeeMaker.Core.Models
{
    public class AzureIotHubOptions
    {
        public string ConnectionString { get; set; }
    }
}
