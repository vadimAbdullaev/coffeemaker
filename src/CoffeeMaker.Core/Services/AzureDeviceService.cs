﻿using CoffeeMaker.Core.Contracts;
using CoffeeMaker.Core.Models;
using Microsoft.Azure.Devices;
using Microsoft.Azure.Devices.Common.Exceptions;
using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;

namespace CoffeeMaker.Core.Services
{
    public class AzureDeviceService : IAzureDeviceService
    {
        private RegistryManager _registryManager;
        private readonly AzureIotHubOptions _options;

        public AzureDeviceService(IOptions<AzureIotHubOptions> options)
        {
            _options = options.Value;
            var iotHubConnectionString = _options.ConnectionString;
            _registryManager = RegistryManager.CreateFromConnectionString(iotHubConnectionString);
        }

        public async Task DeleteDevice(string deviceId)
        {
            if (string.IsNullOrEmpty(deviceId))
                throw new ArgumentException(nameof(deviceId));

            var device = await GetDevice(deviceId);

            if(device != null)
                await _registryManager.RemoveDeviceAsync(device);
        }

        public async Task<Device> GetDevice(string deviceId)
        {
            if (string.IsNullOrEmpty(deviceId))
                throw new ArgumentException(nameof(deviceId));

            var device = await _registryManager.GetDeviceAsync(deviceId);
            
            return device;
        }

        public async Task<Device> Register(string deviceId)
        {
            if (string.IsNullOrEmpty(deviceId))
                throw new ArgumentException(nameof(deviceId));
            try
            {
                var device = await _registryManager.AddDeviceAsync(new Device(deviceId));
                return device;
            }
            catch (DeviceAlreadyExistsException)
            {
                throw new ArgumentException($"{deviceId} already exists");
            }
        }
    }
}
