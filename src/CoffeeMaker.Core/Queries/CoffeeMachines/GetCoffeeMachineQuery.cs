﻿using CoffeeMaker.Dto;
using MediatR;
using System;

namespace CoffeeMaker.Core.Queries.CoffeeMachines
{
    public class GetCoffeeMachineQuery : IRequest<CoffeeMachineDto>
    {
        public Guid CoffeeMachineId { get; set; }
    }
}
