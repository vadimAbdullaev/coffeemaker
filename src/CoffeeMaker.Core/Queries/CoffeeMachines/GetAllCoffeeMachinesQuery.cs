﻿using CoffeeMaker.Dto;
using MediatR;
using System.Collections.Generic;

namespace CoffeeMaker.Core.Queries.CoffeeMachines
{
    public class GetAllCoffeeMachinesQuery : IRequest<IEnumerable<CoffeeMachineDto>>
    {
    }
}
