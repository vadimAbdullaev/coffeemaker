﻿using CoffeeMaker.Core.Models.Dto;
using MediatR;
using System.Collections.Generic;

namespace CoffeeMaker.Core.Queries.Users
{
    public class GetUsersQuery : IRequest<IEnumerable<UserDto>>
    {
    }
}
