﻿using CoffeeMaker.Core.Models.Dto;
using MediatR;

namespace CoffeeMaker.Core.Commands.Users
{
    public class CreateUserCommand : IRequest
    {
        public UserDto UserDto { get; set; }
    }
}
