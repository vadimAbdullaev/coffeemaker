﻿using CoffeeMaker.Dto;
using MediatR;
using System;

namespace CoffeeMaker.Core.Commands.CoffeeMachines
{
    public class CreateCoffeeMachineCommand : IRequest<Guid?>
    {
        public CoffeeMachineDto CoffeeMachineDto { get; set; }
    }
}
