﻿using CoffeeMaker.Dto;
using MediatR;

namespace CoffeeMaker.Core.Commands.CoffeeMachines
{
    public class UpdateCoffeeMachineCommand : IRequest
    {
        public CoffeeMachineDto CoffeeMachineDto { get; set; }
    }
}
