﻿using MediatR;
using System;

namespace CoffeeMaker.Core.Commands.CoffeeMachines
{
    public class DeleteCoffeeMachineCommand : IRequest<bool>
    {
        public Guid CoffeeMachineId { get; set; }
    }
}
