﻿using AutoMapper;
using CoffeeMaker.Core.Contracts;
using CoffeeMaker.Core.Queries.CoffeeMachines;
using CoffeeMaker.Dto;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace CoffeeMaker.Core.QueriesHandlers.CoffeeMachines
{
    public class GetAllCoffeeMachinesQueryHandler : IRequestHandler<GetAllCoffeeMachinesQuery, IEnumerable<CoffeeMachineDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetAllCoffeeMachinesQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<IEnumerable<CoffeeMachineDto>> Handle(GetAllCoffeeMachinesQuery request, CancellationToken cancellationToken)
        {
            var machines = await _unitOfWork.CoffeeMachines.ToListAsync();
            var dtos = _mapper.Map<IEnumerable<CoffeeMachineDto>>(machines);
            return dtos;
        }
    }
}
