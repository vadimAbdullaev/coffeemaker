﻿using AutoMapper;
using CoffeeMaker.Core.Contracts;
using CoffeeMaker.Core.Queries.CoffeeMachines;
using CoffeeMaker.Dto;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace CoffeeMaker.Core.QueriesHandlers.CoffeeMachines
{
    public class GetCoffeeMachineQueryHandler : IRequestHandler<GetCoffeeMachineQuery, CoffeeMachineDto>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetCoffeeMachineQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<CoffeeMachineDto> Handle(GetCoffeeMachineQuery request, CancellationToken cancellationToken)
        {
            var machine = await _unitOfWork.CoffeeMachines
                .FirstOrDefaultAsync(x => x.Id == request.CoffeeMachineId);

            var machineDto = _mapper.Map<CoffeeMachineDto>(machine);

            return machineDto;
        }
    }
}
