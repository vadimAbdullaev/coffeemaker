﻿using AutoMapper;
using CoffeeMaker.Core.Contracts;
using CoffeeMaker.Core.Models.Dto;
using CoffeeMaker.Core.Queries.Users;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace CoffeeMaker.Core.QueriesHandlers.Users
{
    public class GetUsersQueryHandler : IRequestHandler<GetUsersQuery, IEnumerable<UserDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetUsersQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<IEnumerable<UserDto>> Handle(GetUsersQuery request, CancellationToken cancellationToken)
        {
            var users = await _unitOfWork.Users.ToListAsync();
            var result = _mapper.Map<IEnumerable<UserDto>>(users);
            return result;
        }
    }
}
