import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { configurationFactory } from '../core/config';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class DeviceService {
  private baseUrl = 'api/coffeemachine';
  private readonly apiHost = null;

  constructor(private httpClient: HttpClient) {
    this.apiHost = configurationFactory().coffeeApiHost;
  }

  GetDevices(): Observable<any> {
    return this.httpClient.get<any>(`${this.apiHost}/${this.baseUrl}`);
  }

}
