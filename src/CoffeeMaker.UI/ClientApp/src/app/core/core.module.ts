import { NgModule, APP_INITIALIZER } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  OAuthModule,
  ValidationHandler,
  OAuthStorage,
  OAuthModuleConfig,
  OAuthService
} from 'angular-oauth2-oidc';
import { initializeApp, configurationFactory, ServerConfig } from './config';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { LoginComponent } from './pages/login/login.component';
import { LoginCallbackComponent } from './pages/login-callback/login-callback.component';
import { JwksValidationHandler } from 'angular-oauth2-oidc-jwks';
import { DevicesComponent } from './pages/devices/devices.component';

const authModuleConfig: OAuthModuleConfig = {
  // Inject "Authorization: Bearer ..." header for these APIs:
  resourceServer: {
    allowedUrls: [],
    sendAccessToken: false,
  },
};

@NgModule({
  declarations: [
    LoginComponent,
    LoginCallbackComponent,
    DevicesComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    OAuthModule.forRoot(),
  ],
  providers: [
    { provide: ValidationHandler, useClass: JwksValidationHandler },
    { provide: OAuthStorage, useValue: localStorage },
    { provide: APP_INITIALIZER, useFactory: initializeApp, deps: [HttpClient, OAuthService, OAuthModuleConfig], multi: true },
    { provide: OAuthModuleConfig, useValue: authModuleConfig },
    { provide: ServerConfig, useFactory: configurationFactory },
  ],
})
export class CoreModule { }
