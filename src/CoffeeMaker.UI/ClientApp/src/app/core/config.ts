import { HttpClient } from '@angular/common/http';
import { OAuthService, OAuthModuleConfig, AuthConfig, NullValidationHandler } from 'angular-oauth2-oidc';

export class ServerConfig {
  coffeeApiHost: '';
  azureAd: {
    instance: '',
    domain: '',
    tenantId: '',
    clientId: '',
    apiResourceName: ''
  }
}

let configuration: ServerConfig = null;

export function initializeApp(http: HttpClient, oauthService: OAuthService, oAuthModuleConfig: OAuthModuleConfig) {
  return (): Promise<any> => {
    return new Promise<void>((resolve) => {
      http.get<ServerConfig>('api/config').subscribe(async config => {
        configuration = config;
        oAuthModuleConfig.resourceServer.allowedUrls = [configuration.coffeeApiHost];
        oAuthModuleConfig.resourceServer.sendAccessToken = false;

        const issuer = `${configuration.azureAd.instance}/${configuration.azureAd.tenantId}/v2.0`;
        const loginUrl = `${configuration.azureAd.instance}/${configuration.azureAd.tenantId}/oauth2/v2.0/authorize?`;
        const logoutUrl = `${configuration.azureAd.instance}/${configuration.azureAd.tenantId}/oauth2/v2.0/logout`;
        const postLogOutRedirectUri = window.location.origin;
        const apiResource = `api://${config.azureAd.clientId}/${configuration.azureAd.apiResourceName}`;
        const redirectUri = window.location.origin + '/login-callback';
        const silentRefreshUri = window.location.origin + '/silent-refresh';

        const authConfig: AuthConfig = {
          issuer: issuer,
          loginUrl: loginUrl,
          clientId: configuration.azureAd.clientId,
          logoutUrl: logoutUrl,
          redirectUri: redirectUri,
          postLogoutRedirectUri: postLogOutRedirectUri,
          scope: `${apiResource} openid profile user.read`,
          oidc: true,
          silentRefreshRedirectUri: silentRefreshUri,

          sessionChecksEnabled: true,
          showDebugInformation: true,
          clearHashAfterLogin: false,
        };

        oauthService.configure(authConfig);

        oauthService.tokenValidationHandler = new NullValidationHandler();

        oauthService.setupAutomaticSilentRefresh();

        // oauthService retrieves user info and tokens if user is authneticated
        await oauthService.tryLogin();

        resolve();
      });
    });
  };
}

export function configurationFactory(): ServerConfig {
  return configuration;
}
