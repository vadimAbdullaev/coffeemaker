import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private oauthService: OAuthService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.oauthService.initImplicitFlow();

  }

  login() {
    this.oauthService.initImplicitFlow();
  }

}
