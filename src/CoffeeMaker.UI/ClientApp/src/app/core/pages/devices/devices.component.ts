import { Component, Inject, OnInit } from '@angular/core';
import { DeviceService } from 'src/app/services/device.service';

@Component({
  selector: 'app-devices',
  templateUrl: './devices.component.html',
  styleUrls: ['./devices.component.css']
})
export class DevicesComponent implements OnInit {

  public devices: Device[];

  constructor(private deviceService: DeviceService) {
    this.deviceService.GetDevices().subscribe(result => {
      this.devices = result;
    }, error => console.error(error));
  }

  ngOnInit() {
  }

}
