import { HttpClient } from '@angular/common/http';
import { Component, Inject } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent {
  public devices: Device[];

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    http.get<Device[]>(baseUrl + 'weatherforecast').subscribe(result => {
      this.devices = result;
    }, error => console.error(error));
  }
}

interface Device{
  date: string;
  temperatureC: number;
  temperatureF: number;
  summary: string;
}
