interface Device {
  Id: string;
  SerialNumber: string;
  MachineType: string;
  MaintenanceEmployee: string;
  LastMaintenanceDate: Date;
  MaintenanceNumber: number;
  NotInErrorStatus: boolean;
  Status: CoffeeMachineStatus;
  WasteContainerFullness: number;
  ConnectionKey: string;
}

enum CoffeeMachineStatus {
  Initializing = 1,
  ServiceMode,
  HeatingUp,
  Ready,
  Preparing,
  Serving,
  Done
}
