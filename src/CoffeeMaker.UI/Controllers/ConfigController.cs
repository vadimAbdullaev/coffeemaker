﻿using CoffeeMaker.UI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace CoffeeMaker.UI.Controllers
{
    [Route("api/config")]
    public class ConfigController : Controller
    {
        private readonly IOptions<ConfigModel> config;

        public ConfigController(IOptions<ConfigModel> config)
        {
            this.config = config;
        }

        [HttpGet()]
        public IActionResult Get()
        {
            return Ok(config.Value);
        }
    }
}
