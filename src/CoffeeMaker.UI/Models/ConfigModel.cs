﻿namespace CoffeeMaker.UI.Models
{
    public class ConfigModel
    {
        public string CoffeeApiHost { get; set; }
        public AzureAd AzureAd { get; set; }
    }

    public class AzureAd
    {
        public string Instance { get; set; }
        public string Domain { get; set; }
        public string TenantId { get; set; }
        public string ClientId { get; set; }
        public string ApiResourceName { get; set; }
    }
}
