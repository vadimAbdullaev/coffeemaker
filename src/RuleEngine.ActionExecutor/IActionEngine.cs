﻿using RuleEngine.ActionExecutor.Contracts;

namespace RuleEngine.ActionExecutor
{
    public interface IActionEngine
    {
        IActionExecutor Execute(IAction action);
    }
}