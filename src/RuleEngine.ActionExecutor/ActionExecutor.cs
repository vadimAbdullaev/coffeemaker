﻿using RuleEngine.ActionExecutor.Contracts;
using System.Threading.Tasks;

namespace RuleEngine.ActionExecutor
{
    public class ActionExecutor : IActionExecutor
    {
        public Task Execute(IAction action)
        {

            return Task.CompletedTask;
        }

        public void Execute()
        {
            throw new System.NotImplementedException();
        }
    }
}
