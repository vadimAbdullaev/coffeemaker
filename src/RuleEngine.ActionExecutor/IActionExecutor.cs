﻿namespace RuleEngine.ActionExecutor
{
    public interface IActionExecutor
    {
        void Execute();
    }
}