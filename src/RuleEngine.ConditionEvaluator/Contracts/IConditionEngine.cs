﻿namespace RuleEngine.ConditionEvaluator.Contracts
{
    public interface IConditionEngine
    {
        bool Evaluate(ICondition condition, IEventMessage message);
    }
}