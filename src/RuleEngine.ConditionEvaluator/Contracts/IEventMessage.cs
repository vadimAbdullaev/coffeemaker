﻿using System;

namespace RuleEngine.ConditionEvaluator.Contracts
{
    public interface IEventMessage
    {
        public int WasteContainerFullness { get; set; }
        public DateTimeOffset LastMaintenanceDate { get; set; }
    }
}