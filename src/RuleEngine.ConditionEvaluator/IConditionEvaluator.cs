﻿using RuleEngine.ConditionEvaluator.Contracts;

namespace RuleEngine.ConditionEvaluator
{
    internal interface IConditionEvaluator
    {
        bool Check(IEventMessage message);
    }
}
