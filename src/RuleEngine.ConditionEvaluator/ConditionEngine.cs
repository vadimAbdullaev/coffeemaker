﻿using RuleEngine.ConditionEvaluator.Contracts;
using System;
using System.Linq;
using System.Reflection;

namespace RuleEngine.ConditionEvaluator
{
    public class ConditionEngine : IConditionEngine
    {
        public bool Evaluate(ICondition condition, IEventMessage message)
        {
            IConditionEvaluator evaluator = Resolve(condition, typeof(AssemblyMarker));
            return evaluator.Check(message);
        }

        private IConditionEvaluator Resolve(ICondition condition, Type assemblytype)
        {
            string convention = $"{condition.GetType().Name}Evaluator";
            var evaluator = Assembly.GetAssembly(assemblytype).GetType(convention, true);

            if (evaluator.IsAbstract || evaluator.IsInterface || !typeof(IConditionEvaluator).IsAssignableFrom(evaluator))
                throw new NotImplementedException(convention);

            var argumentMatch = evaluator
                .GetConstructors(BindingFlags.Public)
                .Any(x => x.GetParameters()
                    .Any(a => a.Name == condition.GetType().Name));

            if (!argumentMatch)
                throw new NotImplementedException(convention);

            var evaluatorInstance = Activator.CreateInstance(evaluator, condition) as IConditionEvaluator;
            return evaluatorInstance;
        }
    }
}
