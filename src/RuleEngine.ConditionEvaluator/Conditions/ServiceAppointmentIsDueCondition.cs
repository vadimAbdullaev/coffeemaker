﻿using RuleEngine.ConditionEvaluator.Contracts;

namespace RuleEngine.ConditionEvaluator.Conditions
{
    public class ServiceAppointmentIsDueCondition : ICondition
    {
        public int IntervalInDays { get; set; }
    }
}
