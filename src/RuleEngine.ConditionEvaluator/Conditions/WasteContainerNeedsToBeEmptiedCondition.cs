﻿using RuleEngine.ConditionEvaluator.Contracts;

namespace RuleEngine.Conditions
{
    public class WasteContainerNeedsToBeEmptiedCondition : ICondition
    {
        public WasteContainerNeedsToBeEmptiedCondition(int lvlOfWasteContainer)
        {
            LvlOfWasteContainer = lvlOfWasteContainer;
        }

        public int LvlOfWasteContainer { get; internal set; }
    }
}
