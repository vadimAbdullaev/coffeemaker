﻿using RuleEngine.ConditionEvaluator;
using RuleEngine.ConditionEvaluator.Contracts;
using RuleEngine.Conditions;

namespace RuleEngine.ConditionEvaluators
{
    internal class WasteContainerNeedsToBeEmptiedConditionEvaluator : IConditionEvaluator
    {
        private readonly WasteContainerNeedsToBeEmptiedCondition _condition;

        public WasteContainerNeedsToBeEmptiedConditionEvaluator(WasteContainerNeedsToBeEmptiedCondition condition)
        {
            _condition = condition;
        }

        public bool Check(IEventMessage message)
        {
            return _condition.LvlOfWasteContainer < message.WasteContainerFullness;
        }
    }
}