﻿using RuleEngine.ConditionEvaluator.Conditions;
using RuleEngine.ConditionEvaluator.Contracts;
using System;

namespace RuleEngine.ConditionEvaluator.ConditionEvaluators
{
    internal class ServiceAppointmentIsDueConditionEvaluator : IConditionEvaluator
    {
        public ServiceAppointmentIsDueConditionEvaluator(ServiceAppointmentIsDueCondition condition)
        {
            Condition = condition;
        }

        public ServiceAppointmentIsDueCondition Condition { get; }

        public bool Check(IEventMessage message)
        {
            return message.LastMaintenanceDate.AddDays(Condition.IntervalInDays) <= DateTime.UtcNow;
        }
    }
}
